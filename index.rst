.. _startseite:

Konzepte systemnaher Programmierung
===================================

| **Willkommen zur Online-Version des Skripts für KSP von Prof. Dr. Hellwig Geisse.**
| Diese Version wurde von Paul Wilcke erstellt und ist frei auf `GitLab <https://git.thm.de/pabw13/kspdocs>`_ verfügbar.

.. raw:: html

   <hr style="border:1px solid #81000D">

.. admonition:: Feedback erwünscht!
   :class: important

   Bei Kritik, Fehlern oder Typos:

   - Im Praktikum direkt auf mich zukommen
   - Nachricht auf THMConnect (@pabw13:thm.de)
   - E-Mail an paul.wilcke@mni.thm.de (muss nicht formal sein)

.. raw:: html

   <hr style="border:1px solid #81000D">

Upcoming Changes
----------------

.. raw:: html

   <ul style="list-style-type: disc; margin-left: 20px; font-size: 1.2em;">
       <li>Literaturreferenzen hinzufügen zur THM Bibliothek</li>
       <li>Referenzen des Originalskripts ergänzen</li>
   </ul>

.. raw:: html

   <hr style="border:1px solid #81000D">

Inhaltsverzeichnis
------------------

.. toctree::
   :maxdepth: 3
   :numbered:

   content/chapter1.rst
   content/chapter2.rst
   content/chapter3.rst
   content/chapter4.rst
   content/chapter5.rst
   content/chapter6.rst
   content/chapter7.rst
   content/chapter8.rst
   content/chapter9.rst

.. raw:: html

   <hr style="border:1px solid #81000D">
