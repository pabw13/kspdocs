C: Grundlagen
*************
:guilabel:`C`

Die Syntax und Semantik von Java und C ist für viele Sprachkonstrukte ähnlich (zusammengesetzte Anweisungen, Zuweisungen,
if-, while, do-Anweisungen, Rechenausdrücke, Arrays). Wesentliche Unterschiede:

- **C hat keine Klassen** (dafür Verbunde)
- **C kennt keine Referenzen** (dafür aber explizite Zeiger)
- C hat keine automatische Speicherbereinigung
    :math:`\rightarrow`  Speichermanagement liegt beim Programmierer

Ein "C-Programm" ist eine Sammlung von Funktionen. Genau eine davon muss "main" heißen; sie wird beim
Starten des Programms automatisch aktiviert.

Einfachstes C-Programm:

.. code-block:: c

    int main(int argc, char *argv[]) {
        return 0;
    }

.. tip:: Auf dieser Website können sie eigene kleine C-Programme simulieren und visualisieren zum besseren Verständnis

    `C Simulator und Visualisierungstool <https://pythontutor.com/c.html#>`_

Wie liest man Deklarationen? 
============================
**Immer beim Bezeichner starten!**

.. table::

    +-----------------------------------------+-----------------------------------------------------------------------+
    |        **Deklaration**                  |                      **Bedeutung**                                    | 
    +=========================================+=======================================================================+
    | ``int argc;``                           | ``argc`` ist ein Integer                                              |
    +-----------------------------------------+-----------------------------------------------------------------------+
    | ``char str[100];``                      | ``str`` ist ein Array von 100 Zeichen                                 |
    +-----------------------------------------+-----------------------------------------------------------------------+
    | ``int *w;``                             | ``w`` ist ein Zeiger auf einen Integer                                |
    +-----------------------------------------+-----------------------------------------------------------------------+
    | ``char *argv[]``                        | ``argv`` ist ein Array (unspezifischer Größe) von Zeigern auf Zeichen |
    +-----------------------------------------+-----------------------------------------------------------------------+
    | ``int main(int argc, char *argv[]){}``  | ``main`` ist eine Funktion mit zwei Parametern                        |
    |                                         | (ein Integer und ein Array von Zeigern auf Zeichen),                  |
    |                                         | die einen Integer zurück gibt                                         |
    +-----------------------------------------+-----------------------------------------------------------------------+

Bedeutung der Parameter/Rückgabewerte bei ``main``:

- ``argc`` gibt an, wie viele Elemente ``argv`` hat
- ``argv`` enthält Zeiger auf die Elemente (Strings) der Kommandozeile bei Aufruf des Programms
- ``argv[0]`` enthält vereinbarungsgemäß immer den Namen des gerade laufenden Programms
- der Rückgabewert signalisiert **Erfolg** (0) oder **Misserfolg** (!= 0) an das Betriebssystem (der Wert kann von anderen Programmen abgefragt werden)

Nutzen des Compilers und Ausführen eines Programms
==================================================

**Bsp.:** Wir schreiben unser C-Programm in eine Datei mit dem Namen ``tst1.c`` und übersetzen es mit dem Compileraufruf

``gcc -g -Wall -std=c99 -pedantic -o tst1 tst1.c``

dann entsteht das ausführbare Programm ``tst1``. Rufen wir dieses auf mit der Kommandozeile

| ``./tst1 -s 53 hugo``

so ist ``argc=4`` und ``argv`` hat folgende Elemente:

| ``argv [0] = "./ tst1"``
| ``argv [1] = "-s"``
| ``argv [2] = "53"``
| ``argv [3] = "hugo"``

Ausgabe
=======
| ``printf(char *fmt, ...);`` ("Funktionsprototyp")
| ``fmt:`` "Format-String"; wird so ausgegeben, wie er angegeben ist, aber ``%`` wird ersetzt durch den Wert des
| entsprechenden Ausdrucks in ...

**Beispiel:**

.. code-block:: c

    printf("Das ist die %d-te Ausgabe!\n", 2*5);

| gibt aus:

::

    Das ist die 10-te Ausgabe!

+--------+-------------------------------------------------+
| ``%d`` | Ganzzahl in Dezimaldarstellung                  |
+--------+-------------------------------------------------+
| ``%x`` | Ganzzahl in Hexadezimaldarstellung              |
+--------+-------------------------------------------------+
| ``%c`` | Ganzzahl als Character                          |
+--------+-------------------------------------------------+
| ``%s`` | String (Characters bis zum abschließenden '\0') |
+--------+-------------------------------------------------+
| ``%p`` | Pointer (in maschinenabhängiger Darstellung)    |
+--------+-------------------------------------------------+

.. note:: Verfügbar machen mit ``#include <stdio.h>``

Stringvergleich
===============

.. code-block:: c

    int strcmp(char *s1, char *s2);

Vergleicht die Strings ``s1`` und ``s2`` lexikographisch und liefert die Zahl <0, =0, >0 zurück,
wenn ``s1`` vor ``s2`` liegt, gleich ``s2`` ist, oder nach ``s2`` liegt.

.. attention:: **Achtung:** s1==s2 ist syntaktisch auch richtig, vergleicht aber die Zeiger und nicht die Strings!

.. note:: Verfügbar machen mit ``#include <string.h>``

Beenden des Programms
=====================

#. mit ``return ...;`` aus ``main()``
#. mit ``exit(...);`` aus irgendeiner beliebigen Funktion

.. note:: Verfügbar machen mit ``#include <stdlib.h>``


Der C-Präprozessor
==================
.. image:: images/chap2.4-1.png
    :scale: 35
    :align: center

| Befehle an den Präprozessor beginnen mit dem Lattenkreuz **#**
| **Drei wichtige Anwendungen:**

#. **Einschluss ("Inklusion") von Header-Files**

   "Header-Files" (Dateien mit der Endung .h) beinhalten Konstanten und Typdefinitionen, sowie
   Funktionsprototypen. Grund: getrennte Übersetzung von Quelltext-Dateien

   .. image:: images/chap2.4-2.png

   Wenn z.B. in ``src1.c`` ein Aufruf von ``strcmp()`` vorkommt, muss der Compiler beim Übersetzen über
   die Parameter/ Rückgabetypen Bescheid wissen. Wodurch? ``#include <string.h>``. Ebenso z.B.,
   wenn ``src2.c`` eine Funktion aus ``src1.c`` benutzt: ``#include "src1.h"``

   .. attention:: Auch ``src1.c`` muss ``#include "src1.h"`` enthalten, damit die Deklaration in ``src1.h`` und
        die Implementierung in src1.c nachprüfbar übereinstimmen!

   .. note:: ``#include <...>`` und ``#include "..."`` suchen an verschiedenen Stellen nach der inkludierten
        Datei; beide Formen schieben den kompletten Inhalt der Datei in den Übersetzungsprozess der
        inkludierenden Datei ein.

#. **Textersetzung durch Makros**

   (a) Symbolische Namen für Konstante

   .. code-block:: c

    #define ANTWORT_AUF_ALLE_FRAGEN 42
    ...
    if (x == ANTWORT_AUF_ALLE_FRAGEN){...}

   (b) "Inlining" von einfachen, immer wiederkehrenden Rechnungen

   .. code-block:: c

    #define FUNC(x) (3*(x)+1)
    ...
    c = FUNC(a) + FUNC(b)

   .. attention:: Der Präprozessor versteht nichts von Priorität und Assoziativität

    .. code-block:: c

        #define ADD1(x) x+1
        ...
        c = 2*ADD1(3) /* man erwartet 8 */
        /* ist identisch mit */
        c = 2*3+1 /* man erhält 7 */

   .. important:: **Konsequenz: Bei Makrodefinitionen Klammern setzen!**

#. **Bedingte Kompilierung**

   .. code-block:: c

    #define LINUX
    ...
    #ifdef LINUX
    /* wird nur übersetzt, falls LINUX definiert ist */
    #endif

   **Anwendung: Ausschluss von Mehrfachinklusion**

   .. code-block:: c

    #ifndef SRC1_H_INCLUDED
    #define SRC1_H_INCLUDED
    ...
    #endif

