Speicherverwaltung
******************

Aufgaben
========

Speicherallokation
------------------
| Findet statt bei der Erzeugung von Objekten. Methoden:

1. Freiliste: alle freien Bereiche sind verkettet. Vorwiegend verwendet bei konstanter Speichergröße,
   sonst sehr aufwändig (siehe ``malloc`` aus C)

2. Freibereich: der freie Bereich des Speichers ist stets zusammenhängend. Das erfordert "Kompaktierung"
   des Speichers

.. note:: Wir haben unterschiedlich große Objekte :math:`\rightarrow` **Methode 2**

Speicherkompaktierung
---------------------
| Wird zur Erzeugung eines zusammenhängenden Freibereichs durchgeführt. Methoden:

1. separater Kompaktierungslauf

2. zusammen mit Speicherfreigabe (bevorzugt)

Speicherfreigabe
----------------
| Prinzipielle Möglichkeiten:

- explizit (d.h. programmiert: C, C++)
- implizit (d.h. automatisch: Java)

| Explizite Speicherfreigabe birgt ein hohes Risiko der falschen Benutzung ("dangling pointer problem")!
| Methoden zur impliziten Speicherfreigabe:

1. | Referenzzähler
   | **Idee:** Jedes Objekt beinhaltet einen Zähler, dessen Wert die Anzahl der auf das Objekt zeigenden
     Zeiger ist. Fällt der Wert auf 0 :math:`\rightarrow` Speicherplatz ist frei
   |
   | **Vorteil:** gleichmäßige Verteilung des zeitlichen Aufwands
   | **Nachteile:**

   - Aufwand ist hoch: aus jeder Zuweisung ``p1=p2;`` wird ``decRef(p1); p1=p2; incRef(p1);``
   - Speicherverwaltung ist über das gesamte Programm verteilt
   - Zyklische Strukturen werden nicht freigegeben, obwohl sie nicht mehr referenziert werden.

2. | Müllsammeln (Garbage Collection, GC)
   | **Idee:** Zu bestimmten Zeiten (z.B. wenn der freie Speicher knapp ist) wird ermittelt, welche Objekte
     noch erreichbar sind - ausgehend von den Registern und dem Stack der Maschine. Alle anderen
     Objekte werden freigegeben.
   |
   | **Vorteile:**

   - Speicherverwaltung ist sauber abgegrenzt vom Rest der Maschine
   - Zyklische Strukturen werden gesammelt

Verfahren zum Müllsammeln
=========================

Mark-Sweep-Verfahren
--------------------
| Dies sind nicht-kompaktierende Verfahren. Sie laufen in zwei Phasen ab:

1. Phase "Mark": Alle erreichbaren Objekte werden markiert (d.h. ein Flag im Objekt wird gesetzt)
2. Phase "Sweep": Das ist ein Durchgang durch alle Objekte. Der Speicherplatz von nicht markierten
   Objekten wird freigegeben (und das Markierungsflag bei allen Objekten zurückgesetzt)

| Die Mark-Sweep-Verfahren unterscheiden sich nur in der Implementierung der Mark-Phase.

.. rubric:: Rekursiver Depth-First-Durchlauf

.. code-block:: c

    void mark(ObjRef p) {
      if (p-> markFlag) return;
      p->markFlag = TRUE;
      for(each ObjRef q in the object pointed to by p){
        mark(q);
      }
    }

| **Vorteil:** Sehr einfache Implementierung
| **Nachteil:** Platzbedarf des Stacks für Rekursion groß
| (schlechtester Fall: genau so groß wie der gesamte Heap!) :math:`\rightarrow` unbrauchbar.

.. rubric:: Iterativer Depth-First-Durchlauf mit Zeigerumkehr (Dentsch, Schorr, Waite 1965)

| **Idee:** Die Information, wohin man im Graphen "zurück" muss, wird nicht im Stack, sondern im Graphen
  selbst gespeichert.

| **Bsp.:** Binärbaum
| :math:`\Rightarrow` Das ist das bei konstanter Objektgröße vorwiegend verwendete Verfahren.

.. image:: ./images/chap8.2.1.2.png
  :scale: 32
  :align: center

Kopierverfahren
---------------
| Dies sind kompaktierende Verfahren, die einen zusammenhängenden Freibereich erzeugen.

Stop & Copy (Minsky, Fenichel, Yochelson, 1969)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Der Arbeitsspeicher wird in zwei Hälften geteilt
| **"Ziel-Halbspeicher"**: dort werden die Objekte allokiert
| **"Quell-Halbspeicher"**: ist unbenutzt
| System läuft, bis ein Objekt allokiert werden soll, das zu groß für den verbleibenden Platz im
  Ziel-Halbspeicher ist. Situation dann:
|
| Jetzt wird der GC gestartet. Aktionen:

1. Flip (Vertauschen der Halbspeicher)
2. Kopieren der aus den Registern (und dem Stack) der VM verwiesenen Objekte (sog. "Root Objects")
3. Kopieren aller anderen zugänglichen Objekte (der sog. "lebenden" Objekte);
   diese Phase des GC heißt "Scan".

|
| **Problem:**
| Wenn ein Objekt kopiert wurde, wird es Zeiger sowohl im Quell- als auch im Ziel-Halbspeicher
  geben, die noch auf das alte Objekt zeigen. Wie werden diese Zeiger auf das kopierte Objekt geändert?
|
| **Idee:**
| Ein kopiertes Objekt wird gekennzeichnet (durch das "Broken-Heart-Flag") und in ihm wird die
  Adresse des kopierten Objektes gespeichert (der sog. "Forward-Pointer"). Während der Scan-Phase wer-
  den alle Zeiger korrigiert.

.. rubric:: Algorithmus zum Umwandeln eines Zeigers auf ein Objekt im Quellspeicher, in einen Zeiger auf das
    entsprechende Objekt im Zielspeicher:

.. code-block:: c

    ObjRef relocate (ObjRef orig){
      ObjRef copy;
      if(orig == NULL){
        /* relocate(nil) = nil */
        copy = NULL;
      }else if(orig->brokenHeart){
        /* Objekt ist bereits kopiert, Forward-Pointer gesetzt */
        copy = orig->forwardPointer;
      }else{
        /* Objekt muss noch kopiert werden */
        copy = copyObjectToFreeMem(orig);
        /* im Original: setze Broken-Heart-Flag und Forward-Pointer */
        orig->brokenHeart = TRUE;
        orig->forwardPointer = copy;
      }
      /* Adresse des kopierten Objektes zurück */
      return copy;
    }

| Situation nach dem Kopieren der Root-Objekte

.. rubric:: Algorithmus für die Scan-Phase:

.. code-block:: c

    scan = zielhalbspeicher;
    while (scan != freizeiger){
      /* es gibt noch Objekte, die gescannt werden müssen */
      if (Objekt enthält Zeiger){
        for (jeder Zeiger q im Objekt, auf das scan zeigt){
          scan ->q = relocate (scan ->q);
        }
      }
      scan += Größe des Objektes, auf das scan zeigt;
    }

| Am Ende der Scan-Phase befinden sich alle lebenden Objekte im Ziel-Halbspeicher und alle Verweise
  innerhalb von Objekten zeigen in diesen Teil des Speichers.

.. note::
    1. Die Scan-Phase macht einen Breadth-First-Durchgang durch alle Objekte
    2. Nach dem Kopieren eines Objektes ist dessen Inhalt irrelevant; das Broken-Heart-Flag und der
       Forward-Pointer können ihn bedenkenlos überschreiben
    3. In unserer VM eignet sich das zweithöchste Bit von ``size`` als Broken-Heart-Flag und die restlichen
       30 Bits als Forward-Pointer, gespeichert als Byte-Offset relativ zum Beginn des Halbspeichers

| **Nachteil des Stop & Copy-Verfahrens:** Während eines GC-Laufs halten alle anderen Berechnungen an.

GC von Baker (1978)
~~~~~~~~~~~~~~~~~~~

| **Idee:**
| Verteilen der Rechenzeit für die GC (bei jeder Objekt-Allokation wird ein kleiner Teil der Scan-Phase
  abgearbeitet). Aufteilung des Ziel-Halbspeichers:
|
| **Komplikation:**
| Broken-Heart-Objekte zu jeder Zeit vorhanden!

GC von Liebermann und Hewitt (1980)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| **Beobachtung:**
| Bei Baker's GC bleibt die Lebensdauer der Objekte unberücksichtigt
|
| **Idee:**
| Der mittlere Aufwand zur Erzeugung eines Objektes wird geringer, wenn es gelingt kurzlebige
  Objekte "billiger" zu machen
|
| **Realisierung:**
| Aufteilen des Speichers in mehrere kleine "Baker-Regionen". Objekte ähnlichen Alters sin
  in der gleichen Region. Jüngere Regionen werden öfter entmüllt als ältere

| **Vorteil:**

- Sehr viel weniger Kopierarbeit als bei Baker

| **Komplikation:**

- Objektreferenzen über die Regionsgrenzen hinweg müssen behandelt werden

