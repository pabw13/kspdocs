Richtig große Zahlen
********************
:guilabel:`VM`

**Aufgabe**: Berechnung von :math:`\sum_{i=1}^{100} \frac{1}{i}` als exakter Bruch.

Wegen :math:`\frac{1}{a} + \frac{1}{b} = \frac{a+b}{a \cdot b}` kann der Nenner der gesuchten Zahl in der Größenordnung von :math:`100! \approx 10^{158}` liegen (Zahl mit 158 Stellen).

:math:`\Rightarrow` **Ziel:** Rechnen mit beliebig großen Zahlen.

**1. Prinzip**

Ganze Zahlen werden in einem Stellenwertsystem mit Basis :math:`b` dargestellt:

.. math::

   z = \sum_{i=0}^{n} d_i \cdot b^i

Dabei heißen die :math:`d_i` "Ziffern" der Zahl :math:`z`, und es gilt :math:`0 \leq d_i \leq b` für alle :math:`i = 0 ... n`. 

Beim Aufschreiben von :math:`z` reiht man einfach die Ziffern :math:`d_i` aneinander:

.. math::

   z = d_n d_{n-1} \dots d_1 d_0

Beispiel:

.. math::

   b = 10, \quad z = 1954 = 1 \cdot 10^3 + 9 \cdot 10^2 + 5 \cdot 10^1 + 4 \cdot 10^0

**2. Zahlendarstellung**

Wir wählen :math:`b = 256`, sodass jede Ziffer der Zahl in einem Byte dargestellt wird. Die gesamte Zahl belegt dann ein ausreichend großes Array von Bytes.

Bei einer Subtraktion können unvorhersehbar viele Stellen "vernichtet" werden (Null enthalten), die aber nicht gespeichert werden sollen. Die tatsächlich belegte Anzahl von Bytes wird daher mit abgespeichert.

Negative Zahlen? :math:`\rightarrow` Vorzeichen-Betrags-Darstellung!

:math:`\Rightarrow` Der Datentyp für beliebig große Zahlen in C:

.. code-block:: c

    typedef struct {
        int nd;                    /* number of digits */
        unsigned char sign;        /* sign */
        unsigned char digits[1];   /* the digits */
    } Big;

.. hint::

   Für Details wie "was ist das Vorzeichen von 0?" oder "darf die höchste Ziffer 0 sein?", siehe das Paket ``bigint.tar.gz``!

.. note::

   Der Verbund ``Big`` ist in unseren Objekten die "Nutzlast" und wird in der ``data``-Komponente gespeichert.

**3. Bibliotheksinterfaces**

Die Bigint-Bibliothek hat zwei Interfaces:

- **Nach "unten"**: das Memory-Management-Interface

  Neben einer Funktion zur Fehlerausgabe und zum Anhalten des Programms benötigt die Bibliothek eine Funktion, um ein neues Objekt anzulegen:

  .. code-block:: c

     ObjRef newPrimObject(int dataSize);

  Diese Funktion stellt sicher, dass mindestens ``dataSize`` Bytes im Objekt zur Verfügung stehen und gibt eine Objektreferenz zurück.

- **Nach "oben"**: das Benutzer-Programmierinterface

  Hier werden die arithmetischen Operationen bereitgestellt. Man würde also etwa für die Addition erwarten:

  .. code-block:: c

     ObjRef bigAdd(ObjRef op1, ObjRef op2);

.. caution::

  Aufgrund der Garbage-Collection ist dies nicht direkt möglich!

  **Warum?** ``bigAdd()`` wird für das Resultat ein neues Objekt anlegen und deshalb ``newPrimObject()`` aufrufen. Wenn der Speicherplatz dafür nicht ausreicht, wird eine Garbage Collection durchgeführt, bei der alle Objekte im Speicher verschoben werden können. Dadurch werden alle Objektreferenzen ungültig!

  **Lösung**: Objektreferenzen dürfen nur in Variablen gespeichert werden, die dem Garbage Collector bekannt sind und von ihm berichtigt werden.

  .. image:: ./images/chap6.1-1.png
     :scale: 28
     :align: center
  
  **Konsequenz:** 
  
  Es gibt eine globale, strukturierte Variable ``bip`` ("big integer processor"), in die die Argumente von ``bigAdd()`` gespeichert werden (das sind Objektreferenzen). Dann wird die Funktion aufgerufen. Dabe gibt es möglicherweise eine GC und die Referenzen ändern sich. Das ist OK, da ``bip`` dem GC bekannt ist. Das Ergebnis der Operation (ebenfalls eine Objektreferenz) steht danach ebenfalls in ``bip``.
  
  *Details entnehmen Sie bitte dem Paket* :math:`\rightarrow` ``bigint.tar.gz``

**4. Einbinden der Bibliothek**

Man könnte den Quelltext der Bibliothek als ein weiteres Modul zur Ninja-VM hinzufügen. Da die Bibliothek jedoch eine klar abgegrenzte Funktionalität hat und nicht geändert werden sollte, behandelt man sie formal als Bibliothek ("Library").

- **Erzeugen der Bibliothek**

  .. image:: ./images/chap6.1-2.png
     :scale: 28
     :align: center

- **Kompilieren mit:**

  .. code-block:: bash

   gcc -g -Wall -o bigint.o -c bigint.c
   # und dann
   ar -crs libbigint.a bigint.o

  .. note::

     ``ar`` ist der "Archiver" (besser wäre "library manager"). Eine "Library" ist eine Sammlung von Objekt-Dateien (``.o``) und hat einen Namen, der mit ``lib`` beginnt und mit ``.a`` endet.

- **Benutzung der Bibliothek**

  1. Module, die Funktionen aus der Bibliothek verwenden, müssen die entsprechenden Header inkludieren. Die Header müssen jedoch nicht in das Entwicklungsverzeichnis kopiert werden. Stattdessen kann man das Verzeichnis der Header bekannt machen, z.B. mit:

     .. code-block:: bash

        gcc -I ../build/include ...

  2. Beim Binden muss sowohl das Verzeichnis, in dem die Bibliothek liegt, als auch die Bibliothek selbst angegeben werden:

     .. code-block:: bash

        gcc -L ../build/lib ... -lbigint

     | **Achtung, Namenskonvention!**
     | Einbinden von ``libbigint.a`` erfolgt mit ``-lbigint``!
