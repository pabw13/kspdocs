VM: Objekte
***********

Objekte, Objektreferenzen
=========================
| Rechenobjekte bis *jetzt*: ``Integer``, ``Character``, ``Boolean``
| Rechenobjekte *in Zukunft*: Zusammengesetzte Objekte (Records und Arrays, "enthalten" andere Objekte)
|
| **Problem:** Die Lebensdauer der Objekte wird sehr oft größer sein müssen als die Ausführungsdauer der sie
    erzeugenden Prozeduren :math:`\Rightarrow`  Der Stack ist zum Speichern der Objekte ungeeignet!

.. important:: Wir brauchen einen **"Heap"**: ein Speicher, in dem Objekte beliebig lange leben können.

| **Später**: eigene Heap-Verwaltung mit *Garbage-Collector*;
| **Jetzt**: Benutzung von ``malloc`` und keine Benutzung von ``free()``.
| Jedes Objekt wird durch einen Zeiger auf seinen privaten Speicherbereich identifiziert, seine Objektreferenz.


.. rubric:: Wie definiert man einen Typ in C?

:guilabel:`C` Genauso wie eine Variable, aber mit vorangestelltem ``typedef`` .

| **Bsp.:**

1.
   | ``unsigned int U32;`` würde eine Variable mit Namen *U32* vom Typ ``unsigned int`` definieren
   | ``typedef unsigned int U32;`` definiert den Typ mit Namen *U32* als Alias für ``unsigned int``
    (aber keine Variable).

   .. code-block:: c

       typedef unsigned int U32;
       U32 counter; // Variablendefinition mit dem neuen Typ wie üblich

2.
   | ``char *Messages[10];`` würde eine Variable mit Namen *Messages* definieren (ein Feld mit 10 Zeigern
    auf ``char``)
   | ``typedef char *Messages[10];`` definiert den Typ mit Namen *Messages* als Alias für ein Feld mit
    10 Zeigern auf ``char``

   .. code-block:: c

       typedef char *Messages[10];
       Messages messages; // Variablendefinition

.. rubric:: Konventionen zur Namensgebung bei Typen:

1. erster Buchstabe ist Großbuchstabe, z.B. ``Size``
2. angehängtes ``_t`` am Namen, z.B. ``size_t``

.. hint:: Diese Konvention sollte einheitlich durch die Codebasis gehalten werden

.. rubric:: Objekte in der VM

:guilabel:`VM` **Stack:** enthält Objektreferenzen; Heap enthält Objekte

.. image:: ./images/chap5.1-1.png
    :scale: 30
    :align: center

.. rubric:: Vorschlag:


.. code-block:: c

    typedef int Object;        /* Objekt */
    typedef Object *ObjRef;    /* Objektreferenz */

| **Zwei Schwierigkeiten:**

1. Die Objekte werden in Zukunft verschieden groß sein :math:`\rightarrow` Größenangabe im Objekt notwendig.
   Wie macht man das in C?
2. Der Stack muss auch reine Zahlen aufnehmen (``fp``, ``ra``). Wie kann man in C den gleichen
   Speicher zu verschiedenen Zeiten für Daten verschiedenen Typs nutzen?

Verbunde ("Records")
====================
:guilabel:`C`

| ``Feld ("Array")`` : Kollektion von Variablen gleichen Typs, Auswahl durch Zahl ("Index")
| ``Verbund ("Record")`` : Kollektion von Variablen möglicherweise unterschiedlichen Typs, Auswahl durch Namen ("Komponente")
|
| Verbunde gibt es in zwei Ausprägungen:

1. **Fixe Verbunde** ("Structs") bieten Platz für **alle** der aufgezählten Komponenten:

   .. code-block:: c

        struct {
            char name[50];
            int tag;
            int monat;
            int jahr;
        } person;

   definiert eine fixe Verbund-Variable ``person`` mit vier Komponenten. Auswahl einer Komponente
   durch den Punkt-Operator, Bsp.: ``person.jahr = 1954;``

2. **Variable Verbunde** ("Unions") bieten Platz für **irgendeine** der aufgezählten Komponenten:

   .. code-block:: c

        union {
            double d;
            unsigned char b[sizeof(double)];
        } inspect;

   definiert eine variable Verbund-Variable ``inspect``, die Platz für entweder eine ``double``-Größe oder
   ein entsprechend großes Byte-Array hat. Damit kann man z.B. die Darstellung von Fließkomma-Größen
   sichtbar machen:

   .. code-block:: c

        inspect.d = 3.1415926;
        for(i=0; i<sizeof(double); i++) {
            printf("0x%02x", inspect.b[i]);
        }

.. note:: Die Definition von Verbunden geschieht meist in Verbindung mit einer Typdefinition:

    .. code-block:: c

       typedef struct {
          ...
          ...
       } Person;        /* Typdefinition */

       Person person;   /* Variablendefinition */

.. warning:: Das reicht nicht aus für rekursive Definitionen!

   **Bsp.:** Verkettete Liste von ganzen Zahlen

   .. code-block:: c

       typedef struct {
           int number;     /* Geht nicht! Der Bezeichner "Liste" wird verwendet */
           Liste *next;    /* bevor er definiert ist. */
       } Liste;

   Deshalb gibt es sogenannte "tag names": Namen, die nur in Verbindung mit ``struct`` oder ``union``
   gültig sind.

   .. code-block:: c

       typedef struct liste {
           int number;
           struct liste *next;    /* "liste" ist der tag name */
       } Liste;

.. note::
   Häufig werden Verbunde auf dem Heap angelegt und mittels Zeiger zugegriffen. Dann
   beschreibt der Ausdruck ``(*p).next`` die Dereferenzierung des Zeigers ``p`` mit anschließender
   Auswahl der Komponente ``next`` .

   .. image:: ./images/chap5.2-1.png
       :scale: 45
       :align: center

   .. important:: Abkürzung: ``(*p).next`` :math:`\equiv` ``p->next``

Anwendungsbeispiel: Arithmetische Ausdrücke
===========================================

**Bsp:** :math:`(3+4) \cdot 5`

.. image:: ./images/chap5.3-1.png
    :scale: 30
    :align: center

| Was müssen die Knoten speichern?

- innere Knoten: Operation linker Teilbaum, rechter Teilbaum
- Blattknoten: Zahl

| Ein Knoten ist entweder innerer Knoten oder Blattknoten :math:`\Rightarrow` Das wird dargestellt durch eine ``union``.
| Wenn ein Knoten bearbeitet werden soll, muss das Programm "wissen", ob es ein innerer oder ein Blattknoten ist

.. rubric:: Darstellung eines Knoten:

.. code-block:: c

    typedef struct node {
        boolean isLeaf;
        union {
            struct {
                char operation;
                struct node *left;
                struct node *right;
            } innerNode;
            int value;
        } u;
    } Node;

.. rubric:: 1. Erzeugen von Knoten auf dem Heap

.. code-block:: c

    Node *newLeafNode(int value){
        Node *result;
        result = malloc(sizeof(Node));
        if (result == NULL) error("no memory");

        result->isLeaf = TRUE;
        result->u.value = value;

        return result;
    }

    Node *newInnerNode(char operation, Node *left, Node *right){
        Node *result;
        result = malloc(sizeof(Node));
        if (result == NULL) error("no memory");

        result->isLead = FALSE;
        result->u.innerNode.operation = operation;
        result->u.innerNode.left = left;
        result->u.innerNode.right = right;

        return result;
    }

.. rubric:: Beispiel:

.. code-block:: c

    expression =
      newInnerNode (
        '*',
        newInnerNode (
          '+',
          newLeafNode (3),
          newLeafNode (4)
        ),
        newLeafNode (5)
      );

.. rubric:: 2. Auswerten von Knoten

.. code-block:: c

    int eval(Node *tree){
      int op1, op2, result;
      if(tree->isLeaf){
        result = tree->u.value;
      } else {
        op1 = eval(tree->u.innerNode.left);
        op2 = eval(tree->u.innerNode.right);
        switch(tree->u.innerNode.operation){
          case '+':
            result = op1 + op2;
            break;
          case '-':
            ...
          }
        }
      return result;
    }

.. rubric:: Beispiel:

``eval(expression);`` liefert :math:`35`

.. rubric:: 3. Übersetzen von Knoten in VM-Befehle

.. code-block:: c

    void emit(Node *tree){
      if (tree->isLeaf) {
        printf("pushc %d\n", tree->u.value);
      }else{
        emit(tree->u.innerNode.left);
        emit(tree->u.innerNode.right);
        switch(tree->u.innerNode.operation){
          case '+':
            printf("add\n");
            break;
          case '-':
            ...
        }
      }
    }

.. rubric:: Beispiel:

| ``emit(expression)`` liefert

.. code-block::

    pushc 3
    pushc 4
    add
    pushc 5
    mul


Implementation in der NJVM
==========================

:guilabel:`VM`

| Der Stack ist ein statisch angelegtes Array von "Stack-Slots":
| ``StackSlot stack[STACK_SIZE];``
|
| Jeder Stack-Slot muss in der Lage sein, entweder eine Objektreferenz oder eine einfache Zahl aufzunehmen

.. code-block:: c

    typedef struct{
      bool isObjRef;    /* slot used for object reference ? */
      union{
        ObjRef objRef;  /* used if isObjRef=TRUE */
        int number;     /* used if isObjRef=FALSE */
      }u;
    }StackSlot;

.. tip::
  | Um ``bool`` zu benutzen muss man ``<stdbool.h>`` inkludieren.
  |
  | Alternativ kann man auch ``bool isObjRef`` durch ``int isObjRef`` ersetzen, um dann 0 und 1 als
    Wahrheitswerte zu nutzen.

| Die **Static Data Area** ist ein dynamisch angelegtes Array von Objektreferenzen: ``ObjRef *staticData;``

.. image:: ./images/chap5.3-2.png
    :scale: 30
    :align: center

| Objektreferenzen zeigen auf Objekte, die selber ihre Größe und eine Variable Anzahl von Bytes speichern:

.. code-block:: c

    typedef struct {
      unsigned int size;        /* byte count of payload data */
      unsigned char data[1];    /* payload data, size as needed */
    }*ObjRef;

.. rubric:: Wie kann ein Interface zu diesem "Objektspeicher" aussehen?

1. Anlegen:

.. code-block:: c

    objRef = malloc(sizeof(unsigned int) + sizeof(int));
    objRef->size = sizeof(int);
    *(int*)objRef->data = value; // value = eigentliche Daten der Objektreferenz

2. Benutzen:

.. code-block:: c

    *(int*)objRef->data

.. attention::

    Wie viele Bytes "transportiert" die Zuweisung ``n = *(int*)objRef->data`` eigentlich?

    **Antwort:** Der dereferenzierte Zeiger ist ein Zeiger auf int, also werden ``sizeof(int)`` Bytes
    kopiert (bei uns: 4). Die Komponente ``data`` ist aber als Array von einem Byte erklärt - wieso
    funktioniert das überhaupt?

    Zwei Gründe:

    1. Wir haben beim Anlegen genügend viele Bytes angelegt
    2. C macht keine Prüfung auf Einhalten der Arraygrenzen

    .. tip:: Konsequenz :math:`\Rightarrow` "size as needed" funktioniert.
