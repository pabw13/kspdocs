Lose Enden
**********

Zwei Bedeutungen des Schlüsselwortes ``static``
===============================================
1. Bei Variablendeklarationen innerhalb von Funktionen ("lokalen Variablen") bewirkt ``static``
   das Anlegen des Speicherplatzes für die Variable im (statischen) Datensegment, nicht im Stack (wie
   sonst bei lokalen Variablen). Damit bleibt der Speicherplatz und der Wert darin über den Aufruf
   der Funktion hinaus erhalten.

.. rubric:: Beispiel:

| Eine Funktion soll mitzählen, wie oft sie aufgerufen wurde, ohne eine globale Variable zu benutzen.

.. code-block:: c

    void f(void) {
      static int n = 0;
      n++;
      printf("ich wurde %d mal aufgerufen\n", n);
    }

.. attention:: Initialisierung der statischen Variablen passiert nur einmal

2. | Bei Variablendefinitionen außerhalb von Funktionen ("globale Variable") und bei Funktionsdefinitionen
     beschränkt ``static`` die Sichtbarkeit des Namens auf die Quelltextdatei, in der die Definition
     steht.
   | Verwendung: Verbergen von nur lokal gebrauchten Namen.

   .. code-block:: c

        #include <stdio.h>

        static int counter;
        static int f(int x){ ... }

        void doIt(int what){ ... }

   .. important:: Nach außen ist nur die Funktion ``doIt()`` sichtbar.

Zeigerarithmetik
================
| Wenn :math:`p` ein Zeiger auf ein Objekt vom Typ :math:`T` ist und :math:`n` eine ganze Zahl, dann ist
  :math:`p \pm n` ein Zeiger auf ein Objekt vom Typ :math:`T`, das sich :math:`n` Objekte (nicht Bytes!)
  weiter hinten (also vorne) im Speicher befindet.
|
| Natürlich können die so errechneten Zeiger auch dereferenziert werden: ``*p(+2)`` liefert das Objekt
  ``a[4]`` (wenn ``p``  auf den Index :math:`2` zeigt).

.. image:: ./images/chap9.2-1.png
  :scale: 30
  :align: center

.. tip::
  | Erinnerung: ``a`` :math:`\equiv` ``&a[0]``, also ein Zeiger aufs erste Element.
  | :math:`\Rightarrow` ``a[n]`` :math:`\equiv` ``*(a+n)`` und ``&a[n]`` :math:`\equiv` ``a+n``
  | Das gilt für beliebige Zeiger auf ``a``!
  |
  | Typische Anwendung: Mitführen von Zeigern statt Indizierung.

  .. rubric:: Beispiel: Kopierschleife

  .. code-block:: c

      while(n--) *q++ = *p++;

Funktionszeiger
===============
| Wenn ``T f(...){..}`` eine Funktionsdefinition ist, dann ist ``f`` ein Zeiger auf diese Funktion.
  Dieser Zeiger kann als Argument in eine Funktion hineingereicht werden, oder auch in einer Variable
  abgespeichert werden: ``p=f;``
| Dabei muss die Variablendeklaration so aussehen: ``T (*p)(..);``
| Soll die an ``p`` hängende Funktion aufgerufen werden, schreibt man ``(*p)(...)``

.. rubric:: Typische Anwendung: Auswahl einer Funktion aus einer Menge von Funktionen zur Laufzeit

.. code-block:: c

    int inspect(int arg){...}
    int simulate(int arg){...}
    ...

    typedef struct {
      char *name;
      int (*fkt)(int arg);
    }Command;

    Command cmdlist [] = {
      {"anzeigen", inspect},
      {"simulieren", simulate},
      ...
    };

    int execute(char *cmdname, int arg) {
      int i;
      for (i=0; i<sizeof(cmdlist) / sizeof(cmdlist[0]); i++) {
        if (strcmp(cmdname, cmdlist[i].name) == 0) {
          /* command found */
          return (*cmd[i].fkt)(arg);
        }
      }
      /* command not found */
      error("command not found");
      return -1;
    }


Funktionen mit variabel vielen Argumenten
=========================================
| Deklaration mit drei Punkten in der Parameterliste, z.B. ``int printf(char *fmt, ...);``
| Zugriff auf die Argumente innerhalb der Funktion durch einen Satz von Makros, definiert in ``<stdarg.h>``.
|
| Typische Verwendung: eigene Fehlerausgabe, soll so verwendet werden können wie ``printf``.

.. code-block:: c

    void error(char *fmt, ...){
      va_list ap;
      va_start(ap, fmt );
      printf("Error: ");
      vprintf(fmt, ap);
      printf("\n");
      va_end(ap);
      exit(1);
    }

.. tip:: Aufruf z.B. mit ``error("unbekannte Instruktion %x an Adresse %x", code[pc], pc);``
