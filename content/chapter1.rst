
Einführung
**********

Warum KSP?
==========
1. Embedded Systems
2. Hochperformante Systeme bei knappsten Ressourcen (Speicherplatz, Ausführungszeit)
3. Programme nahe an (Realzeit-) Betriebssystem, manchmal völlig ohne Betriebssystem direkt auf der Hardware
4. Am häufigsten eingesetzte Sprache: **C**
5. Plattformabhängige Realisierung von plattformunabhängigen Konzepten durch "Virtuelle Maschinen" 
   (Beispiel: **Java**)

Themen
======
1. **C-Programmierung**
2. **Speicherverwaltung, Laufzeitorganisation von Programmen**
3. **Bibliotheken**
4. **Werkzeugkette** (Compiler, Assembler, Binder/Linker, Lader/Loader)
5. **Garbage-Collectoren**
6. **Interpreter, Virtuelle Maschinen**

Aufbau der Veranstaltung
========================
**Zwei Alternativen:**

- **linear:** 4 Wochen C-Programmieren, 3 Wochen Werkzeugkette, ...
- **projektzentriert:** anhand eines Projektes lernt man alle notwendigen Konzepte, Tools, Verfahren etc. kennen

| **Vorteil von a):** sehr systematisch  
| **Vorteil von b):** man lernt, warum die Dinge so sind, wie sie sind und es macht mehr Spaß!
|
| Meine Wahl: **b)**!

Projekt
=======
Konstruktion einer Virtuellen Maschine zur Ausführung von übersetzten Programmen der kleinen
Programmiersprache Ninja ("Ninja is not Java")

.. image:: images/chap1-4-1.png
    :scale: 50
    :align: center

.. note::

   - Das ist in kleinem Maßstab auch das, was bei Java passiert.
   - Konstruktion *bottom-up*: Wir beginnen mit einer minimalen VM und nehmen Features dazu, wenn benötigt.

Literatur
============
#. | **The C Programming Language**, 2nd Edition, Prentice Hall, 2012:
   | *Brian W. Kernighan, Dennis M. Ritchie*
   | `Verfügbar in der THM Bibliothek <https://hds.hebis.de/thm/Record/HEB089780515>`_
#. | **Computer Systems: A Programmer's Perspective**, Prentice Hall, 2015:
   | *Randal E. Bryant, David O'Hallaron*
#. | **Garbage Collection**, John Wiley & Sons, 1996:
   | *Richard Jones, Rafael Lins*
#. | **Virtual Machines**, Morgan Kaufmann, 2005:
   | *James E. Smith, Ravi Nair*
#. | **The Garbage Collection Handbook**:
   | *Richard Jones, Antony Hosking, Eliot Moss*
   | `Verfügbar in der THM Bibliothek als eBook <https://hds.hebis.de/thm/Record/HEB516792032>`_

Symbole zur Einordnung des Lehrstoffs
=====================================
:guilabel:`C` die Programmiersprache C

:guilabel:`VM` die virtuelle Maschine für Ninja

:guilabel:`ASM` der Assembler für Ninja

:guilabel:`NJ` die Programmiersprache Ninja
