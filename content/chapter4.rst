VM: Variable & Kontrollstrukturen
*********************************

Variable
========
:guilabel:`VM`

| Eine Variable ist ein Ort, an dem ein Wert aufgehoben ("gespeichert") werden kann.
| In Java gibt es vier Arten von Variablen:

.. code-block:: java

    class C {
        static int clsVar;          // Klassenvariable
        int instVar;                // Instanzvariable
        void m(int paramVar) {      // Parametervariable
            int localVar;           // Lokale Variable
            ...
        }
    }


.. rubric:: In C ebenso:

+-------------------+----------------------------+
| Java              | C                          |
+===================+============================+
| Klassenvariable   | globale/statische Variable |
+-------------------+----------------------------+
| Instanzvariable   | Variable auf dem Heap      |
+-------------------+----------------------------+
| Parametervariable | Parametervariable          |
+-------------------+----------------------------+
| Lokale Variable   | Lokale Variable            |
+-------------------+----------------------------+


"global" vs. "statisch"
=======================
| "global" bezeichnet die Sichtbarkeit der Variablen: sie sind von überall her zugreifbar
| "statisch" bezeichnet die Lebensdauer der Variablen: sie lebt (hat Speicherplatz) die ganze Programm-
    laufzeit

.. important::
    global :math:`\rightarrow`  statisch, aber statisch :math:`\not\Rightarrow`  global (siehe Klassenvariablen in Java)


Globale Variablen, Static Data Area
===================================

| Globale Variablen werden in der "Static Data Area" (**=SDA**) gespeichert. Jede Variable gibt es genau einmal.
    Sie wird eindeutig bezeichnet durch ihre Adresse in der Static Data Area.
| Zum Arbeiten mit globalen Variablen gibt es zwei Instruktionen:
|
| ``push global variable`` und ``pop global variable``
|
| Beide haben als *Immediate-Wert* die *Adresse der Variablen* kodiert.

.. code-block:: c

    pushg<n> ...        ->  ... wert (legt Wert der SDA an Stelle n auf den Stack)
    popg<n>  ... wert   ->  ... (speichert top-of-stack in SDA an Stelle n)

.. rubric:: **Beispiel:**

| ``x`` soll an der Adresse :math:`2`  und
| ``y`` an der Adresse :math:`5`
| in der Static Data Area gespeichert sein.
|
| Dann kann die Anweisung :math:`x=3 \cdot x+y` berechnet werden durch die Instruktionsfolge:

.. code-block:: c

    pushc  3
    pushg  2
    mul
    pushg  5
    add
    popg   2

Lokale Variablem, Stack Frames
==============================
| Lokale Variablen (und Parametervariablen) muss es getrennt für jeden Prozedur- und Funktionsaufruf
  ("Aktivierung") geben (wegen Rekursion).
| Also können diese Variablen nicht statisch gespeichert werden, sondern bekommen ihren Platz auf dem Stack
 (neu für jede Aktivierung).
| Der Zugriff über ``sp`` ist schwierig, da der sich dauernd ändert.
|
| Deshalb ein neues Register der VM: ``fp`` (Framepointer)

.. image:: ./images/chap4.4-1.png
    :scale: 30
    :align: center

| Der Bereich zwischen ``fp`` und ``sp`` heißt "aktueller Rahmen" und speichert die lokalen Variablen der
  momentan ausgeführten Prozedur/Funktion/Methode. Beim Aufruf einer Prozedur muss ein neuer Rahmen angelegt
  werden, der beim Verlassen wieder vernichtet wird.
|
| Dazu zwei neue Instruktionen:
| ``allocate stack frame`` und
| ``release stack frame``

.. code-block::

    asf<n>      ≡       push(fp)
                        fp = sp
                        sp = sp + n

    rsf         ≡       sp = fp
                        fp = pop()

.. note:: Also wird der *"alte"* ``fp`` im Stack selber aufgehoben!

.. rubric:: Beispiel für asf 4

.. image:: ./images/chap4.4-2.png
    :scale: 40
    :align: center

| Mit ``rsf`` wird der Rahmen wieder freigegeben.

Arbeiten mit lokalen Variablen
==============================
| **Zwei neue Befehle:**
| ``push local variable``
| ``pop  local variable``
| Beide haben als Immediate Wert den Abstand zu ``fp`` kodiert.

.. code-block:: c

    pushl<n> ...        ->  ... wert (legt Wert der lokalen Variable auf den Stack)
    popl<n>  ... wert   ->  ... (speichert top-of-stack in lokaler Variable)

.. rubric:: Beispiel

| Angenommen ``x`` soll an der Stelle :math:`2` und ``y`` an der Stelle :math:`5` im aktuellen Rahmen
  gespeichert sein.
| Dann kann die Anweisung :math:`x=3 \cdot x+y` berechnet werden durch die Instruktionsfolge:

.. code-block:: c

    pushc  3
    pushl  2
    mul
    pushl  5
    add
    popl   2

Berechnung von arithmetischen Ausdrücken
========================================
:guilabel:`NJ` und :guilabel:`ASM` und :guilabel:`VM`

.. rubric:: Beispiel

| :math:`a - 3 \cdot b - 5`
|
| **Algorithmus zum Erzeugen der Instruktion:**

1. Schreibe den Ausdruck vollständig geklammert hin (berücksichtige dabei die Priorität und Assoziativität der Operatoren)

    | **Bsp.:** :math:`((a-(3 \cdot b))-5)`

2. Wandle den vollständig geklammerten Ausdruck in einen Baum um:

    (a) Jeder geklammerte Ausdruck ("Operatorausdruck") wird zu einem inneren Knoten des Baumes
    (b) Jede Zahl und jede Variable wird zu einem Blatt des Baumes

.. image:: ./images/chap4.6-1.png
    :scale: 30
    :align: center

|

3. Traversiere den Baum einmal (depth first, post-order)

  (a) Bei einem inneren Knoten:

    i. Traversiere den linken Teilbaum
    ii. Traversiere den rechten Teilbaum
    iii. Gib die zum Operator gehörende arithmetische Instruktion aus

  (b) Bei einem Blattknoten:

    i. Steht an dem Blatt eine Zahl :math:`\rightarrow` Gib ``pushc <zahl>`` aus
    ii. Steht an dem Blatt eine Variable :math:`\rightarrow`
        Gib ``pushl <Abstand des Speicherplatzes zu fp>`` oder ``pushg <Adresse>`` aus

.. rubric:: Beispiel:

| :math:`a - 3 \cdot b - 5`

    :math:`\text{Angenommen, dass } Abstand(a) = 10 \text{ und } Abstand(b) = 14`

    .. code-block:: c

        pushl  10   // Abstand(a)
        pushc  3
        pushl  14   // Abstand(b)
        mul
        sub
        pushc  5
        sub

Zuweisung
=========

.. rubric:: Beispiel

:math:`b = a - 3 \cdot b - 5`

    | **Algorithmus zum Erzeugen der Instruktionen:**

    1. Erzeuge Code für die rechte Seite der Zuweisung
    2. Gib ``popl <Abstand des Speicherplatz zu fp>`` oder ``popg <Adresse>`` aus

|

    :math:`\text{Angenommen, dass } Abstand(a) = 10 \text{ und } Abstand(b) = 14`

    .. code-block:: c

        pushl  10   // Abstand(a)
        pushc  3
        pushl  14   // Abstand(b)
        mul
        sub
        pushc  5
        sub         // bis hier identisch mit obigen Beispiel
        popl   14   // <- Zuweisung in b

Kontrollstrukturen
==================
:guilabel:`NJ` und :guilabel:`ASM` und :guilabel:`VM`

Einarmiges if
-------------
``if (B) S``

.. image:: ./images/chap4.8.1.png
    :scale: 25
    :align: center

.. rubric:: Notation

| ``<B>`` ist die Übersetzung von ``B`` in Assembler
| ``<S>`` ist die Übersetzung von ``S`` in Assembler

.. code-block::

        <B>         ; hinterlässt Ergebnis auf dem Stack
        brf L       ; springe zu L, wenn Ergebnis == false
        <S>
    L:              ; das nennt man ein "Label" oder "Marke", symbolische Form einer Adresse
        ...

.. note::
    | Der bedingte Sprung ``brf`` nimmt den Booleschen Wert vom Stack.
    | Wie kommt der Boolesche Wert auf den Stack? Was ist ``B``?

    .. rubric:: Dazu sechs arithmetische Vergleiche:

    .. code-block::

        a==b    ->     eq ... a b      ->     ... a==b
        a!=b    ->     ne ... a b      ->     ... a!=b
        a<b     ->     lt ... a b      ->     ... a<b
        a<=b    ->     le ... a b      ->     ... a<=b
        a>b     ->     gt ... a b      ->     ... a>b
        a>=b    ->     ge ... a b      ->     ... a>=b

    .. important::
        | Wie wollen wir Boolesche Werte repräsentieren?
        | Besonders einfach: ``0`` :math:`\equiv` ``false``, ``1`` :math:`\equiv` ``true`` (ganze Zahlen)

Zweiarmiges if
--------------
``if (B) S1 else S2``

.. image:: ./images/chap4.8.2.png
    :scale: 30
    :align: center

.. code-block::

        <B>         ; Ergebnis auf dem Stack
        brf L1      ; springe zu L1, wenn Ergebnis == false
        <S1>        ; wird ausgeführt, falls B true
        jmp L2      ; weiter bei L2
    L1:
        <S2>        ; wird ausgeführt, falls B false
    L2:
        ...

while-Schleife
--------------
``while (B) S``

.. image:: ./images/chap4.8.3.png
    :scale: 30
    :align: center

.. rubric:: Alternative 1:

.. code-block::

    L1:
        <B>
        brf L2
        <S>
        jmp L1
    L2:

.. attention:: Nachteil: Zwei Sprünge pro Schleifendurchlauf

.. rubric:: Alternative 2:

.. code-block::

        jmp L2
    L1:
        <S>
    L2:
        <B>
        brt L1

do-Schleife
-----------
``do S while (B)``

.. image:: ./images/chap4.8.4.png
    :scale: 40
    :align: center

| Auch hier gibt es zwei Alternativen, aber nur eine sinnvolle:

.. code-block::

    L:
        <S>
        <B>
        brt L

.. rubric:: **Zusammenfassung**

1. sechs arithmetische Vergleiche liefern Boolesche Werte
2. Sämtliche Kontrollstrukturen (mit Ausnahme des Prozeduraufrufs) werden mit ``jmp/brf/brt`` realisiert.
   Das Sprungziel wird in Assembler durch eine Marke repräsentiert; in der VM ist das die Adresse der
   Instruktion, die am Sprungziel steht.

Auswertung Boolescher Ausdrücke
===============================
| Sprache schreibt vor, ob "vollständige Auswertung" oder "Kurzschlussauswertung":

1. Vollständige Auswertung

   (a) ``B1 && B2``

   .. code-block::

        <B1>
        <B2>
        and

   (b) ``B1 || B2``

   .. code-block::

        <B1>
        <B2>
        or

   .. attention:: Nachteil: ``if (x != 0 && y / x < 5) {...}`` macht nicht das, was es soll!

2. Kurzschlussauswertung

   (a) ``B1 && B2``

   .. code-block::

        <B1>
        brf L1
        <B2>
        jmp L2
    L1:
        pushc 0     ; false
    L2:

   (b) ``B1 || B2``

   .. code-block::

        <B1>
        brt L1
        <B2>
        jmp L2
    L1:
        pushc 1     ; true
    L2:

   .. note::
    | Ninja benutzt **Kurzschlussauswertung**.
    | Der Compiler erzeugt aber eine etwas modifizierte Instruktionsfolge.

Unterprogrammaufruf und Rücksprung
==================================
Ein Unterprogramm ("Call") muss eine Adresse des nächsten Befehls ("Rückkehradresse") speichern,
damit der Unterprogrammrücksprung ("Return") dorthin zurückfindet. Speicherort ist der Stack.

.. code-block:: c

    call  ...        ->  ... ra     // ra = Return Address
    ret   ... ra     ->  ...

.. rubric:: 1. Call ohne Argumente, ohne Rückgabewert

| **Caller** (= aufrufende Prozedur):
| ``call <proc addr>``
|
| **Callee** (= aufgerufene Prozedur):

.. code-block::

    asf <num locals>
    <body>
    rsf
    ret

| **Stackaufbau:** (gezeichnet nach Ausführung von ``asf 3``)

.. image:: ./images/chap4.10-1.png
    :scale: 30
    :align: center

.. rubric:: 2. Call mit Argumenten, aber ohne Rückgabewert

| Die Werte von Argumentausdrücken müssen vor dem Aufruf einer Prozedur berechnet und gespeichert werden. Wo? Auf dem Stack.
|
| Wir nummerieren im Folgenden die :math:`n` Argumente von :math:`0` ("1. Argument", links) bis :math:`n-1` ("n-tes Argument", rechts)

.. note::
    | "Links" und "Rechts" bezieht sich auf die Position der Argumente im Funktionskopf beim Aufruf
    | ``func(3,9,27)`` :math:`\rightarrow` ``0. Arg = 3``, ``1. Arg = 9``, ``2. Arg = 27``


| **Caller:**

.. code-block::

    <push arg 0>
    ...
    <push arg n-1>
    call <proc addr>
    drop <n>            ; löscht n Einträge vom Stack

| **Callee:**

.. code-block::

    asf <num locals>
    <body>
    rsf
    ret

| Stackaufbau

.. image:: ./images/chap4.10-2.png
    :scale: 30
    :align: center

| Zugriff auf Argument ``i (i=0 ... n-1)`` durch negativen Offset zum Framepointer:
| Argument ``i <--> stack[fp - 2 - n + i]``
| Der Wert von ``-2 - n + i`` wird als Immediate-Konstante in die Instruktion ``pushl`` und ``popl`` kodiert:
| so wird auf die Parameter einer Prozedur zugegriffen.

.. rubric:: 3. Call ohne Argumente, aber mit Rückgabewert

| Der Rückgabewert entsteht (als Wert eines Ausdrucks) auf dem Stack. Er muss auch auf dem Stack
   erscheinen, allerdings viel tiefer im Stack: vor der Rückkehradresse!
| **Bsp.:** ``1+2*f()``, mit ``f(){return 5;}``

.. image:: ./images/chap4.10-3.png
    :scale: 30
    :align: center

| Einfachste Lösung: Rückgabe des Wertes in einem zusätzlichen Spezialregister ``r`` mit zwei Instruktionen

.. code-block:: c

    pushr    ...        ->  ... rv   // rv = Return Value
    popr     ... rv     ->  ...

| **Caller:**

.. code-block::

    call <proc addr>
    pushr

| **Callee:**

.. code-block::

    asf <num locals>
    <body>
    <push ret val>
    popr
    rsf
    ret

.. rubric:: 4. Call mit Argumenten und Rückgabewert: Kombination aus 2. und 3.

| **Caller:**

.. code-block::

    <push arg 0>
    ...
    <push arg n-1>
    call <proc addr>
    drop n
    pushr

| **Callee:**
| genau so wie 3., Zugriff auf Argumente: genau so wie 2.
