Die VM
******
:guilabel:`VM`

Stackmaschine
=============

.. image:: ./images/chap3.1-1.png
    :scale: 30
    :align: center

| Rechenoperationen: nehmen die obersten zwei Einträge vom Stack, ermitteln das Ergebnis und legen es
    auf dem Stack ab.
|
| Auswertung von :math:`2 \cdot 3 + 5`

.. code-block::

    pushc   2
    pushc   3
    mul
    pushc   5
    add

| Auswertung von :math:`2 \cdot (3 + 5)`

.. code-block::

    pushc   2
    pushc   3
    pushc   5
    add
    mul

.. attention:: Alle Operanden werden genau in der Reihenfolge ihres Auftretens auf den Stack gelegt!

Instruktionen für die VM
==========================

.. code-block::

    add   ... n1 n2  ->     ... n1+n2
    entsprechend sub, mul, div, mod (Rest beim Dividieren)

    pushc<n> ...     ->     ... n
    halt  ...        ->     ... (hält die VM an)
    rdint ...        ->     ... n (liest eine Ganzzahl)
    wrint ... n      ->     ... (schreibt eine Ganzzahl)
    rdchr ...        ->     ... n (liest ein Zeichen)
    wrchr ... n      ->     ... (schreibt ein Zeichen)

Kodierung von Instruktionen
===========================

Wir stellen eine Instruktion in einer ganzen Zahl mit 32 Bits ohne Vorzeichen dar  ("unsigned int"):

.. image:: ./images/chap3.3-1.png
    :align: center
    :scale: 33

.. table:: Opcode und Immediate-Wert erklärt
    :align: center

    +----------------------+-----------------------------+
    | **Opcode:**          |   legt die Operation fest   |
    +----------------------+-----------------------------+
    | **Immediate-Wert**   | Parameter für die Operation |
    +----------------------+-----------------------------+

Beispiel für pushc 5
--------------------

| Der Opcode für **pushc** ist ``1`` und der **Immediate-Wert** beträgt ``5``.
|
| Also ist der Code für ``"pushc 5"`` :math:`\rightarrow` ``(1 << 24) | 5`` mit (``<<`` = linksschieben, ``|`` = bitweises oder)

.. danger::
    | Was passiert bei ``pushc -1`` ?
    | (**Hinweis:** Zweierkomplement)
    | :math:`\Rightarrow` **Also besser:** ``(1 << 24) | (5 & 0x00FFFFFF)`` (``&`` bitweises und)

Programmspeicher und PC
==========================

Der PC ("ProgramCounter") ist der Index der nächsten auszuführenden Instruktion.

.. image:: ./images/chap3.4-1.png
    :scale: 30

Steuerwerk/Instruktionszyklus
=============================
.. code-block::

    while (!halt) {
      IR = program_memory[PC];  // Instruktion holen in IR (=Instruktionsregister)
      PC = PC + 1;              /* PC inkrementieren */
      exec(IR);                 /* Instruktion ausführen */
    }

:guilabel:`ASM`

| Bis jetzt: Fester Programmspeicherinhalt ("ROM")
| Ab jetzt: Laden eines beliebigen Binärprogramms in die VM

.. image:: ./images/chap3.5-1.png
    :scale: 28

|
| ``prog1.asm`` enthält Text, z.B.

.. code-block::

    rdint
    pushc 2
    mul
    wrint
    halt

``prog1.bin`` enthält Binärdaten: Diese können nicht mehr "direkt" angeschaut werden.

.. admonition:: Wie kommt z.B. ``prog1.bin`` in den Programmspeicher?

    Laden mittels Dateioperationen.

Dateioperationen
================
:guilabel:`C`

- Öffnen einer Datei
- Lesen/Schreiben der Datei, evtl. mit Positionieren
- Schließen einer Datei
- Durch das Einbinden von ``#include <stdio.h>`` stehen die folgende Funktionen zur Verfügung

1. **Öffnen**

.. code-block:: c

    FILE *fopen(char *path, char *mode);

+-------------+------------------------------------------------------------------------------------------------------------------+
| ``path``    | Pfad zur Datei, z.B. "``prog1.bin``"                                                                             |
+-------------+------------------------------------------------------------------------------------------------------------------+
| ``mode``    | Modus, ob gelesen, geschrieben oder angehängt werden soll, z.B. "``r``" für "read"                               |
+-------------+------------------------------------------------------------------------------------------------------------------+
| ``FILE``    | Datenstruktur, die den Zustand der geöffneten Datei hält. Wie das genau aussieht, bleibt dem Benutzer verborgen. |
+-------------+------------------------------------------------------------------------------------------------------------------+
| ``fopen()`` | liefert ``NULL`` zurück, falls es fehlschlägt. Das muss immer überprüft werden!                                  |
+-------------+------------------------------------------------------------------------------------------------------------------+

----

2. **Schließen**

.. code-block:: c

    int fclose(FILE *fp);

| Leert die Schreib-/Lesepuffer und gibt die ``FILE``-Struktur an die Speicherverwaltung zurück.
| Danach darf der Inhalt von ``fp`` nicht mehr benutzt werden; der Zeiger zeigt auf nicht mehr verfügbaren Speicher!

----

3. **Lesen (& Schreiben)**

.. code-block:: c

    size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);

+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter  | Bedeutung                                                                                                                                                             |
+============+=======================================================================================================================================================================+
| ``ptr``    | Zeiger auf Puffer, der vom Benutzer zur Verfügung zu stellen ist.                                                                                                     |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``size``   | Größe eines zu lesenden Datenobjekts in Bytes. Diese kann durch den statisch auswertbaren Operator ``sizeof`` berechnet z.B. ``sizeof(int)`` (=4 bei 32-Bit-Rechnern) |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``nmemb``  | Anzahl der Datenobjekte, die gelesen werden sollen. Deshalb insgesamt benötigter Platz im Puffer: ``size * nmemb``                                                    |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``stream`` | der von ``fopen`` gelieferte Filepointer                                                                                                                              |
+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. attention::
    | Rückgabewert der Funktion ``fread``:
    | Anzahl der gelesenen Datenobjekte, nicht Zahl der gelesenen Bytes!

.. tip::
    | Warum ``void *`` bei ``ptr``? ``fread`` soll für beliebigen Datentypen in beliebiger Anzahl funktionieren.
    | ``void *`` :math:`\equiv`  Zeiger auf irgendwas
    | **Konsequenz**: Der Compiler weiß nichts über Typ oder Größe

.. rubric:: Bsp.: Lesen von 10 Integern aus einer Datei xy.z

.. code-block:: c

    FILE *fp;
    int feld [10];
    fp = fopen("xy.z", "r");
    if (fp == NULL) {
        printf("...");
        exit(99);
    }
    if (fread (&feld[0], sizeof(int), 10, fp) != 10) {
        printf("...");
        exit(99);
    }
    ...

.. hint::
    | & :math:`\equiv` Adressoperator, gelesen: "Adresse von ..."
    |
    | ``&feld[0]`` :math:`\equiv` Adresse von ``feld[0]``
    | Kurzschreibweise: ``feld`` :math:`\equiv` ``&feld[0]``

.. image:: ./images/chap3.6-1.png
    :scale: 30
    :align: center

Adressoperator & Zeigererklärung
--------------------------------

| Datentyp von ``&feld[0]``: ``int *``, Zeiger auf Integer
|
| Allgemein: ``d`` hat Typ ``T`` :math:`\Rightarrow` ``&d`` hat Typ ``T*``.
| Das nennt man Referenzieren (Zeigerkonstruktion).
|
| Der umgekehrte Vorgang heißt Dereferenzieren:
| ``p`` hat Typ ``T*`` :math:`\Rightarrow` ``*p`` hat Typ ``T``


| **Bsp.:**
| ``*(&feld[5])`` :math:`\equiv` ``feld[5]`` hat Typ ``int``

Speicheranforderung und Freigabe
================================

| Lokale Variablen in C sind automatische Variablen, d.h. sie haben ihren Speicherplatz auf dem
    C-Laufzeitstack.
| **Bsp.:** ``int f (void) {int a,b; ...}``
|
| Ihr Speicherplatz wird automatisch freigegeben, wenn die Funktion verlassen wird, in der sie deklariert
    sind.

.. note:: Deshalb ist es ein grober Fehler, einen Zeiger auf eine lokale Variable zurückzugeben:

    .. code-block:: c

        int *f(void){
            int i = 5;
            return &i; /* Das ist ein Fehler!! */
        }

| Wenn man Datenobjekte anlegen möchte, die länger leben, als die Funktion, die sie anlegt, muss das mit
| ``void *malloc(size_t size);``
| auf dem Heap geschehen.

Adressraum eines laufenden C-Programms:

.. image:: ./images/chap3.7-1.png
    :scale: 30
    :align: center

| Freigabe des Speichers mit
| ``void free(void *p);``

