Objekte in Objekten
*******************

Objekte
=======
:guilabel:`VM`

| Objekte "beinhalten" andere Objekte, indem sie Variablen zum Speichern von Objektreferenzen bereitstellen:

.. image:: ./images/chap7.1-1.png
    :scale: 30
    :align: center

.. rubric:: Beispiel

| ``A`` ist Instanz eines "Punktes" mit den Koordinaten ``B`` und ``C``
| (``B`` und ``C`` sind "primitive Objekte": nicht zerlegbar).
|
| **Objekte gibt es in zwei Varianten:**

1. | "Record" (in OO-Sprachen "Instanz einer Klasse" genannt)
   | **Merkmal:** Zugriff auf Variablen durch Namen

2. | "Array" (heißt in OO-Sprachen genauso)
   | **Merkmal:** Zugriff auf Variablen durch (berechneten) Index

Beiden ist gemeinsam: sie speichern (unter Umständen, viele) Objektreferenzen

Objekte: Darstellung
====================
.. attention::
    | Wir haben ein **Problem:**
    | Es gibt zwei ganz verschiedene Sorten von Objekten (primitive und zusammengesetzte!)
    | Wie halten wir die auseinander?

.. rubric:: Beispiel

1. ein primitives Objekt mit der Zahl ``0x12345678``
2. ein Record mit einer Instanzvariablen, die zufällig mit der Objektreferenz (=Adresse)
   ``0x12345678`` belegt ist. Beide sehen im Speicher einer Little-Endian-Maschine so aus:

.. image:: ./images/chap7.2-1.png
    :scale: 40
    :align: center

.. rubric:: Wir vereinbaren:

(a) Primitive Objekte bleiben so, wie oben beschrieben.
(b) Bei zusammengesetzten Objekten (Records und Arrays) wird das höchste Bit der Größe auf 1
    gesetzt, und die restlichen Bits zählen die Objektreferenzen, nicht die Bytes!

.. rubric:: Beispiel:

| Der Record von oben sieht damit so aus:

.. image:: ./images/chap7.2-2.png
    :scale: 35
    :align: center

.. rubric:: Hier ein paar nützliche Makros:

(a) Ist das Objekt ein primitives Objekt?

.. code-block:: c

    #define MSB         (1 << (8 * sizeof(unsigned int) - 1))
    #define IS_PRIM     (objRef) (((objRef)->size & MSB) == 0)

(b) Wie viele Objektreferenzen beinhaltet das Objekt?

.. code-block:: c

    #define GET_SIZE(objRef) ((objRef)->size & ~MSB)

(c) Berechne einen Zeiger auf die erste Objektreferenzen in Objekt!

.. code-block:: c

    #define GET_REFS(objRef) ((ObjRef*)(objRef)->data)

Objekte: Referenzen
===================
:guilabel:`NJ`

.. rubric:: Definition

.. code-block:: c

    type Point = record {
      Integer x;
      Integer y;
    };

.. rubric:: Erzeugen

.. code-block:: c

    local Point p;
    p = new(Point);

.. rubric:: Zugriff

.. code-block:: c

    k = p.x;
    p.y = 2 * k;

:guilabel:`ASM & VM`

| Erzeugen: ``new <n> ... -> ... object`` (``n`` ist die Anzahl der Objektreferenzen im Objekt; oben: 2)
| Zugriff: ``(...).x`` Objekt und Komponente ("Instanzvariable")

Das Objekt entsteht als Ergebnis einer Berechnung auf dem Stack. Die Komponente gibt an, wo im
Objekt der Zugriff erfolgen soll: es ist also die Nummer der Instanzvariable (oben: ``x->0``, ``y->1``).
Der Name und damit die Nummer ist beim Übersetzen bekannt; sie wird als Immediate-Wert in der Instruktion
kodiert:

.. code-block::

    getf <i> ... objekt         ->      ... value   ("get field")
    putf <i> ... objekt value   ->      ...         ("put field")

Objekte: Arrays
===============

:guilabel:`NJ`

.. rubric:: Definition

.. code-block:: c

    type Vector = Integer[];

.. rubric:: Erzeugen

.. code-block:: c

    local Vector v;
    v = new(Integer[2*n+1]);

.. attention:: Komplikation: Anzahl der Elemente zur Laufzeit!

.. rubric:: Zugriff

.. code-block:: c

    k = v[i];
    v[n-i] = 2 * k;

.. attention:: Komplikation: Indexberechnung zur Laufzeit!

:guilabel:`ASM & VM`

| Erzeugen: ``newa ... nelem  -> ... array`` (``nelem`` ist die Anzahl der Objektreferenzen im Array)
| Zugriff: ``(...)[...]`` Objekt und Index
|
| Beides (Objekt und Index) sind Ergebnisse von Berechnungen, liegen also auf dem Stack

.. code-block::

    getfa   ... array index           ->      ... value   ("get field of array")
    putfa   ... array index value     ->      ...         ("put field of array")

| Wir haben also dynamische Arrays :math:`\rightarrow` man sollte zur Laufzeit die Größe eines Arrays feststellen können!

:guilabel:`NJ`

| Wir vereinbaren also für beliebige Objekte

.. math::

    sizeof(...) = \left\{
    \begin{array}{ll}
    -1 & \text{für ein primitives Objekt} \\
    \text{Anzahl der Objektreferenzen} & \text{für ein zusammengesetztes Objekt}
    \end{array}
    \right.

| Beachte den Unterschied:
| In C wird ``sizeof(...)`` statisch ausgewertet (Compilezeit)
| In Ninja wird ``sizeof(...)`` dynamisch ausgewertet (Laufzeit)

:guilabel:`ASM & VM`

.. code-block::

    getsz   ... object      ->      ... size   ("get size")

.. attention::
    | Implementierung ist einfach, da jedes Objekt seine Größe speichert.
    | Primitive Objekte sollen jedoch -1 liefern!

Nil, Initialisierungen, Laufzeittests
=====================================
| In Java gibt es ``null``, in Ninja gibt es ``nil``: eine Referenz, die auf kein Objekt verweist.

.. important::
    Die VM sollte alle Instanzvariablen neuer Objekte sowie alle lokalen Variablen von Funktionen
    (bei Ausführung von ``asf``) und alle globalen Variablen (direkt nach dem Anlegen) mit ``nil``
    initialisieren.

| Einfachste Darstellung von ``nil`` in C: der Nullzeiger ``NULL``. Dann sollte die VM jeden Zugriff
  auf ein Objekt prüfen und mit einem Fehler abbrechen, wenn die Objektreferenz ``nil`` ist.
| Zusätzlich muss die VM bei Zugriffen auf Arrays den Index prüfen :math:`(0 \leq i \leq size)`
  und ggf. mit Fehler abbrechen.
| Sie kann dies auch bei Records tun (wenn der Compiler den Code erzeugt hat, das ist aber unnötig).

Referenzvergleiche
==================

| Um Bedingungen wie z.B. ``x == NIL`` berechnen zu können, brauchen wir 3 neue Instruktionen:

.. code-block::

    pushn ...       ->   ... nil    // legt nil auf den Stack
    refeq ... x y   ->   ... b      // prüft Referenzen auf Gleichheit
    refne ... x y   ->   ... b      // prüft Referenzen auf Ungleichheit

.. note:: Mit den beiden letzten Instruktionen kann man Objekte auf Identität prüfen

.. image:: ./images/chap7.6-1.png
    :scale: 35
    :align: center
