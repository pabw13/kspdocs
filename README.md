# KSPDocs

## Fertige Website:
[KSPDocs gehostet als THM GitLab Page](https://pabw13.git-pages.thm.de/kspdocs/index.html)


## Installation und lokaler Build

### Optional: Python-Umgebung einrichten
Falls erwünscht, kann eine virtuelle Umgebung mit einem Tool Ihrer Wahl erstellt werden (z.B. conda).

### Schritte zur Installation und Erstellung der Dokumentation
1. Installieren Sie die benötigten Abhängigkeiten:
   ```bash
   pip install -r requirements.txt
   ```
2. Erstellen Sie die Dokumentation als Website:
   ```bash
   make html
   ```
   Optional können Sie auch andere Formate wie LaTeX, EPUB oder JSON erstellen. Für eine Übersicht kann der folgende Befehl genutzt werden.
   ```bash
   make
   ```
   Beispielsweise für LaTeX:
   ```bash
   make latex
   ```

Die fertige Website finden Sie anschließend unter:
```
_build/html/index.html
```

## Feedback und Kritik

Für Feedback, Fehlerberichte oder Verbesserungsvorschläge bitte einen Pull Request oder Issue erstellen. Sie können mich auch direkt kontaktieren über THMConnect (@pabw13:thm.de) oder per E-Mail an paul.wilcke@mni.thm.de. Ihre Nachricht muss nicht formell sein.
